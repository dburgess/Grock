# Grock

![](demo-images/icon.png)

GTK application to display geological maps of the UK, powered by [libchamplain](https://gitlab.gnome.org/GNOME/libchamplain).

Base map tiles provided by [Mapbox](https://www.mapbox.com/), geological data provided by the [British Geological Survey](https://www.bgs.ac.uk)'s various [Web Map Services](https://www.bgs.ac.uk/data/services/wms.html).

Built with GNOME Builder.

## Screenshots

![](demo-images/demo.png)

## How to use

* Location button to go to your current location (_will likely not work yet_)
* Menu allows toggling satellite view / gravity anomalies / magnetic anomalies / linear features
* Opacity of geological overlay may also be changed
* Long tap / right click to bring up information about rock type 

## To do

- [X] Rock information on right-click
- [ ] Find current location
- [X] Satellite view
- [X] Make usable on mobile
- [ ] Refactor and clean up code

## License

Grock is licensed under the GNU GPLv3 Licence - see [COPYING](COPYING).

## Many thanks to

* Maintainers of and contributors to the project's dependancies.
* Contributors to gnome-recipes, gnome-maps, gnome-photos and Glade. Inspection of the source code of these programs was useful in learning how to build a GTK program.
* The many people willing to help out on the _Gnome Newcomers_ Matrix channel.
