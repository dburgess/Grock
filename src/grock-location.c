/* grock-location.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libgeoclue-2.0/geoclue.h>
#include <glib-2.0/glib.h>

#include "grock-config.h"
#include "grock-location.h"
#include "grock-globals.h"

static GClueSimple *simple = NULL;
static GClueClient *client = NULL;
static gint         time_threshold;

GrockLocation
grock_location_get_location (void)
{
  GrockLocation ret = {0.0, 0.0};

  g_return_val_if_fail (GCLUE_IS_SIMPLE (simple), ret);

  GClueLocation *location = gclue_simple_get_location (simple);

  ret.lat = gclue_location_get_latitude (location);
  ret.lon = gclue_location_get_longitude (location);

  return ret;
}

static gboolean
on_location_timeout (gpointer user_data)
{
  g_clear_object (&client);
  g_clear_object (&simple);

  return FALSE;
}

static void
on_client_active_notify (GClueClient *client, GParamSpec *pspec,
                         gpointer user_data)
{
  if (gclue_client_get_active (client))
    return;

  g_print ("Geolocation disabled.");

  on_location_timeout (NULL);
}

static void
on_location_ready (GObject *source_object, GAsyncResult *result,
                   gpointer user_data)
{
  GError *error = NULL;

  simple = gclue_simple_new_finish (result, &error);

  if (error != NULL)
    {
      g_critical ("Failed to connect to Geoclue2 service: %s", error->message);

      return;
    }

  client = gclue_simple_get_client (simple);
  g_object_ref (client);

  if (time_threshold > 0)
    gclue_client_set_time_threshold (client, time_threshold);

  g_signal_connect (simple, "notify::active",
                    G_CALLBACK (on_client_active_notify), NULL);
}

void
grock_location_init (void)
{
  gclue_simple_new (PACKAGE_ID, GCLUE_ACCURACY_LEVEL_EXACT, NULL,
                    on_location_ready, NULL);
}
