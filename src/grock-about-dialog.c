/* grock-about-dialog.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib/gi18n.h>
#include <gtk/gtk.h>

#include "grock-about-dialog.h"
#include "grock-config.h"
#include "grock-globals.h"

struct _GrockAboutDialog
{
  GtkAboutDialog  parent_instance;

  GtkWidget      *grock_logo;
};

G_DEFINE_TYPE (GrockAboutDialog, grock_about_dialog, GTK_TYPE_ABOUT_DIALOG)

static void
grock_about_dialog_init (GrockAboutDialog *self)
{

}

static void
grock_about_dialog_class_init (GrockAboutDialogClass *class)
{

}

void
grock_about_dialog_show (GrockAboutDialog *self, GtkWindow *window)
{
  gtk_show_about_dialog (window, NULL);
}

GrockAboutDialog *
grock_about_dialog_new (void)
{
  GrockAboutDialog *about_dialog;

  const char *authors[] = {
    "Daniel Burgess",
    NULL
  };

  g_autoptr(GdkPixbuf) logo = NULL;

  GError *err = NULL;

  logo = gdk_pixbuf_new_from_resource ("/me/leucoso/Grock/me.leucoso.Grock.png", &err);

  logo = gdk_pixbuf_scale_simple (logo, 256, 256, GDK_INTERP_NEAREST);

  if (err != NULL)
    g_print ("Error loading logo for about dialog: %s\n", err->message);

  about_dialog = g_object_new (GROCK_TYPE_ABOUT_DIALOG,
                        "program-name", _("Grock"),
                        "version", PACKAGE_VERSION,
                        "copyright", "© 2019 – 2020 Daniel Burgess",
                        "license-type", GTK_LICENSE_GPL_3_0,
                        "comments", _("Geological Map of the UK"),
                        "authors", authors,
                        "logo-icon-name", PACKAGE_ID,
                        "website", "https://codeberg.org/dburgess/grock",
                        "website-label", _("Visit source repository"),
                        NULL);

  gtk_widget_realize (GTK_WIDGET (about_dialog));

  gdk_window_set_functions (gtk_widget_get_window (GTK_WIDGET (about_dialog)),
                            GDK_FUNC_ALL | GDK_FUNC_MINIMIZE | GDK_FUNC_MAXIMIZE);

  return about_dialog;
}
