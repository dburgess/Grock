/* grock-search-popover.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_SEARCH_POPOVER_H
#define GROCK_SEARCH_POPOVER_H

#include <gtk-3.0/gtk/gtk.h>

typedef struct
{
  GtkWidget *pop;

  GtkWidget *main_grid;
  GtkWidget  *main_stack;
  GtkWidget   *spinner;
  GtkWidget   *lbl_no_results;
  GtkWidget   *scrolled_window;
  GtkWidget    *list_box;

  GList *places_list;
  GtkWidget *result_labels[10];
  GtkListBoxRow *list_box_rows[10];

} GrockSearchPopover;

GrockSearchPopover *grock_search_popover_make     (GtkSearchEntry *widget);
void                grock_search_popover_populate (GrockSearchPopover *p, GList *places_list);

#endif
