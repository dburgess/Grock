/* grock-search.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include <geocode-glib-1.0/geocode-glib/geocode-glib.h>
#include <gtk-3.0/gtk/gtk.h>
#include <proj.h>

#include "grock-features.h"
#include "grock-search.h"
#include "grock-search-popover.h"

/*
// TODO: Make sure the places_list is freed with g_list_free()
static void
on_results_recieved (GObject *object, GAsyncResult *result, GrockSearchPopover *p)
{
  GError *error = NULL;

  GList *places_list = geocode_forward_search_finish (GEOCODE_FORWARD (object), result, &error);

  if (error != NULL)
    {
      g_message ("Failed to search for location: %s\n", error->message);
      return;
    }

  for (GList *l = places_list; l != NULL; l = l->next)
    {
      g_print ("Place: %s\n", geocode_place_get_name (l->data));
    }

  grock_search_popover_populate (p, places_list);
}


void
grock_search_perform_search (GtkSearchEntry *entry, GrockSearchPopover *p)
{
  const gchar *query = gtk_entry_get_text (GTK_ENTRY (entry));

  GeocodeForward *forward = geocode_forward_new_for_string (query);

  // Limit searches to the UK
  GeocodeBoundingBox *bbox = geocode_bounding_box_new (60.866269, 49.846369, -8.344021, 2.697705);
  geocode_forward_set_search_area (forward, bbox);
  geocode_forward_set_bounded (forward, TRUE);

  geocode_forward_set_answer_count (forward, 10);

  geocode_forward_search_async (forward, NULL, on_results_recieved, p);
}*/

static void
remove_spaces (gchar *string)
{
  const gchar *i = string;

  do
    {
      while (*i == ' ')
        ++i;
    } while (*string++ = *i++);
}

static GrockPoint
get_latlon_from_bng (GrockPoint point)
{
  //const gchar *epsg3857 = "EPSG:3857";

  const gchar *bng = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs";
  const gchar *wgs84 = "EPSG:4326";

  PJ_CONTEXT *proj_context = proj_context_create ();
  PJ *proj = proj_create_crs_to_crs (proj_context, bng, wgs84, NULL);

  if (proj == 0)
    g_error ("Error transforming coordinates.\n");

  PJ *proj_for_gis = proj_normalize_for_visualization (proj_context, proj);

  if (proj_for_gis == 0)
    g_error ("Error transforming coordinates.\n");

  proj_destroy (proj);
  proj = proj_for_gis;

  PJ_COORD bng_coord = proj_coord (point.lon, point.lat, 0, 0);

  PJ_COORD wgs84_coord = proj_trans (proj, PJ_FWD, bng_coord);

  GrockPoint ret = {wgs84_coord.lp.lam, wgs84_coord.lp.phi};
  return ret;
}

GrockPoint
grock_search_get_lat_lon_from_reference (gchar *reference)
{
  remove_spaces (reference);

  GString *ref_str = g_string_new (reference);
  g_string_ascii_up (ref_str);

  gchar l1 = ref_str->str[0] - 'A';
  gchar l2 = ref_str->str[1] - 'A';

  if (l1 > 7)
    l1--;
  if (l2 > 7)
    l2--;

  GrockPoint error_ret = {0, 0};

  if (l1 < 8 || l1 > 18)
    {
      g_debug ("Invalid grid reference provided.\n");
      return error_ret;
    }

  gint e100km = ((l1 - 2) % 5) * 5 + (l2 % 5);
  gint n100km = (19 - (gint)floor (l1 / 5) * 5) - (gint)floor (l2 / 5);

  gint length = strlen (ref_str->str);

  if (length % 2 != 0 || length < 2 || length > 12)
    {
      g_debug ("Invalid grid reference provided.\n");
      return error_ret;
    }

  GString *half_easting = g_string_new (g_strndup (ref_str->str + 2, (length - 2) / 2));
  GString *half_northing = g_string_new (g_strndup (ref_str->str + 2 + (length - 2) / 2, (length - 2) / 2));

  // pad the half-strings with zeros
  while (half_easting->len < 5)
    g_string_append (half_easting, "0");
  while (half_northing->len < 5)
    g_string_append (half_northing, "0");

  // convert the half-strings to integers
  guint64 easting;
  guint64 northing;
  GError *error = NULL;

  g_ascii_string_to_unsigned (half_easting->str, 10, 0, 99999, &easting, &error);
  if (error != NULL)
    g_error ("Error converting string to int: %s", error->message);

  error = NULL;
  g_ascii_string_to_unsigned (half_northing->str, 10, 0, 99999, &northing, &error);
  if (error != NULL)
    g_error ("Error converting string to int: %s", error->message);

  gdouble easting_d = easting + (e100km * 100000);
  gdouble northing_d = northing + (n100km * 100000);

  g_print ("Easting: %.0f, northing: %.0f\n", easting_d, northing_d);

  GrockPoint point = {easting_d, northing_d};

  g_string_free (ref_str, TRUE);

  GrockPoint ret = get_latlon_from_bng (point);

  g_print ("Latitude: %f, longitude: %f\n\n", ret.lat, ret.lon);

  return ret;
}

