/* grock-features.c
 *
 * Copyright © 2020 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>

#include <glib-2.0/glib.h>
#include <libsoup/soup.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <proj.h>

#include "grock-config.h"
#include "grock-features.h"
#include "grock-features-popover.h"
#include "grock-globals.h"

static double
lat2y_m (double lat)
{
  return log (tan (DEG2RAD (lat) / 2 + M_PI/4)) * EARTH_RADIUS;
}

static double
lon2x_m (double lon)
{
  return DEG2RAD (lon) * EARTH_RADIUS;
}

GrockTile
grock_features_get_tile_point_from_coords (gdouble lat, gdouble lon,
                                           gint zoomlevel)
{
  gdouble merc_x = lon2x_m (lon);
  gdouble merc_y = lat2y_m (lat);

  gint tile_size = (2.0 * 20037508.0) / pow (2, zoomlevel);

  // Half-width (in tiles) of the map at current zoomlevel
  gint tile_offset = 20037508.0 / tile_size;

  GrockTile ret = {0, 0};

  ret.x = (gint)floor (merc_x / tile_size) + tile_offset;
  ret.y = (gint)floor (merc_y / tile_size) + tile_offset - 1;

  return ret;
}

GrockPoint
grock_features_get_web_mercators_from_coords (gdouble lat, gdouble lon)
{
  GrockPoint ret;

  ret.lon = lon2x_m (lon);
  ret.lat = lat2y_m (lat);

  return ret;
}

static SoupSession *
get_soup_session (void)
{
  gchar user_agent[20];
  GString *version = g_string_new (PACKAGE_VERSION);
  GString *name =    g_string_new (PACKAGE_NAME);
  name = g_string_ascii_down (name);
  g_snprintf (user_agent, 20, "%s/%s", name->str, version->str);

  g_print ("%s\n\n\n\n", user_agent);

  return soup_session_new_with_options ("max-conns", 3, "user-agent", user_agent, NULL);
}

static gchar *
get_features_uri (gdouble lat, gdouble lon, gdouble lat1, gdouble lon1)
{
  gchar *base = FEATURES_BASE_URI;

  static gchar ret[500];
  g_snprintf (ret, 500, "%s%f,%f,%f,%f", base, lon, lat, lon1, lat1);

  return ret;
}

GrockFeatureInfo *
grock_feature_info_new (void)
{
  return (GrockFeatureInfo *)g_malloc0 (sizeof (GrockFeatureInfo));
}

static void
grock_featureinfo_populate (GrockFeatureInfo *info,
                            gchar *heading,
                            gchar *min_specific_time,
                            gchar *max_specific_time,
                            gchar *min_period,
                            gchar *max_period,
                            gchar *rock_type,
                            gchar *setting,
                            gchar *setting_plus,
                            gchar *environment)
{
  info->heading = g_strdup (heading);
  info->min_specific_time = g_strdup (min_specific_time);
  info->max_specific_time = g_strdup (max_specific_time);
  info->min_period = g_strdup (min_period);
  info->max_period = g_strdup (max_period);
  info->rock_type = g_strdup (rock_type);
  info->setting = g_strdup (setting);
  info->setting_plus = g_strdup (setting_plus);
  info->environment = g_strdup (environment);
}

static void
on_xml_received (GObject *object, GAsyncResult *result, GrockFeaturesPopover *p)
{
  GInputStream *stream;
  GError *error = NULL;
  GrockFeatureInfo *info = grock_features_popover_get_feature_info (p);

  stream = soup_session_send_finish (SOUP_SESSION (object), result, &error);

  if (error != NULL)
    {
      g_error ("Error downloading feature information: %s\n", error->message);
      return;
    }

  gchar *buffer = g_malloc0 (4096);
  error = NULL;
  g_input_stream_read (stream, buffer, 4096, NULL, &error);

  xmlDocPtr doc;
  xmlNodePtr cursor;
  doc = xmlReadMemory (buffer, 4096, "noname.xml", NULL, 0);
  if (doc == NULL)
    {
      g_error ("Failed to parse XML.\n");
      goto fail;
    }

  cursor = xmlDocGetRootElement (doc);

  if (cursor == NULL)
    {
      g_error ("Empty XML document.\n");
      goto fail;
    }

  if (xmlStrcmp (cursor->name, (const xmlChar *) "FeatureInfoResponse"))
    {
      g_error ("Invalid XML response: root node is not a FeatureInfoResponse.\n");
      goto fail;
    }

  cursor = cursor->xmlChildrenNode;

  if (cursor == NULL)
    goto fail;

  while (cursor != NULL)
    {
      if (!xmlStrcmp (cursor->name, (const xmlChar *) "FIELDS"))
        {
          gchar *heading =           (gchar *)xmlGetProp (cursor, (const xmlChar *)"LEX_RCS_D"     );
          gchar *min_specific_time = (gchar *)xmlGetProp (cursor, (const xmlChar *)"MIN_TIME_D"    );
          gchar *max_specific_time = (gchar *)xmlGetProp (cursor, (const xmlChar *)"MAX_TIME_D"    );
          gchar *min_period =        (gchar *)xmlGetProp (cursor, (const xmlChar *)"MIN_PERIOD"    );
          gchar *max_period =        (gchar *)xmlGetProp (cursor, (const xmlChar *)"MAX_PERIOD"    );
          gchar *rock_type =         (gchar *)xmlGetProp (cursor, (const xmlChar *)"TYPE_D"        );
          gchar *setting =           (gchar *)xmlGetProp (cursor, (const xmlChar *)"SETTING_D"     );
          gchar *setting_plus =      (gchar *)xmlGetProp (cursor, (const xmlChar *)"SETTINGPLUS_D" );
          gchar *environment =       (gchar *)xmlGetProp (cursor, (const xmlChar *)"ENVIRONMENT_D" );

          xmlFreeDoc (doc);
          xmlCleanupParser ();

          grock_featureinfo_populate (info,  heading,
                                             min_specific_time,
                                             max_specific_time,
                                             min_period,
                                             max_period,
                                             rock_type,
                                             setting,
                                             setting_plus,
                                             environment);

          grock_features_popover_display_info (p);

          return;
        }
      cursor = cursor->next;
    }

fail:
  xmlFreeDoc (doc);
  xmlCleanupParser ();
}

/**
 * This function takes coordinates in EPSG:3857 and returns a structure
 * containing details about the local geology.
 */
void
grock_features_get_from_epsg3857 (gdouble lat, gdouble lon, GrockFeaturesPopover *p)
{
  GrockFeatureInfo *info = grock_features_popover_get_feature_info (p);

  /**
   * Check we're in the UK.
   */
  g_assert (lat > 5414691.645640 && lat < 10211684.489360);
  g_assert (lon > -1945261.298110 && lon < 1134858.947510);

  /**
   * Define coordinates of the top right of the bounding box we use to make the
   * GetFeatureInfo request. The bottom left is (lon, lat).
   */
  gdouble lat1 = lat + 7000;
  gdouble lon1 = lon + 7000;

  info->lat = lat;
  info->lon = lon;

  gchar *uri = get_features_uri (lat, lon, lat1, lon1);

  SoupSession *session = get_soup_session ();
  SoupMessage *msg = soup_message_new ("GET", uri);

  GCancellable *cancel = g_cancellable_new ();
  soup_session_send_async (session, msg, cancel, on_xml_received, p);
}

/**
 * Function courtesy of Chris Veness, see
 * https://github.com/chrisveness/geodesy/blob/35a4380569c86c03008c826db752f17a39a58c1b/osgridref.js#L207
 *
 * The string returned must be freed.
 */
static gchar *
get_grid_ref (gdouble easting, gdouble northing)
{
  const gint precision = 10; // TOTAL number of numbers to have in the grid reference.

  // 100 km grid indices
  gint e100km = (gint)floor (easting / 100000);
  gint n100km = (gint)floor (northing / 100000);

  // convert 100 km indices into the required letters
  gchar letter_1 = (19 - n100km) - (19 - n100km) % 5 + ((e100km + 10) / 5);
  gchar letter_2 = (19 - n100km) * 5 % 25 + e100km  % 5;

  // account for missing letter I on the grid
  if (letter_1 > 7)
    ++letter_1;
  if (letter_2 > 7)
    ++letter_2;

  letter_1 += 'A';
  letter_2 += 'A';

  // remove 100km grid indices and set the required precision
  gint e = (gint)floor (((gint)easting % 100000) / (gint)pow (10, 5 - precision / 2));
  gint n = (gint)floor (((gint)northing % 100000) / (gint)pow (10, 5 - precision / 2));

  gchar *ret = g_malloc0 (20);
  g_snprintf (ret, 20, "%c%c %05d %05d", letter_1, letter_2, e, n);

  return ret;
}

void
grock_features_get_bng_from_epsg3857 (gdouble northing, gdouble easting, GrockFeaturesPopover *p)
{
  const gchar *epsg3857 = "EPSG:3857";
  const gchar *bng = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs";

  GrockFeatureInfo *info = grock_features_popover_get_feature_info (p);

  PJ_CONTEXT *context = proj_context_create ();
  PJ *proj = proj_create_crs_to_crs (context, epsg3857, bng, NULL);

  /**
   * This ensures that coordinates are always returned as longitude
   * THEN latitude for geographic CRSs and easting THEN northing for
   * projected CRSs.
   */
  PJ *proj_corrected = proj_normalize_for_visualization (context, proj);
  proj_destroy (proj);
  proj = proj_corrected;

  if (proj == 0)
   {
     g_debug ("Failed to convert to EPSG:3857 to British National Grid coordinates.\n");
     info->bng_string = g_strdup ("Error retrieving BNG coordinates");
     return;
   }

  PJ_COORD coord_epsg3857 = proj_coord (easting, northing, 0, 0);

  PJ_COORD coord_bng = proj_trans (proj_corrected, PJ_FWD, coord_epsg3857);

  proj_destroy (proj_corrected);
  proj_context_destroy (context);

  info->bng_string = get_grid_ref (coord_bng.enu.e, coord_bng.enu.n);
}
