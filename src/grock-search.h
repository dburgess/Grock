/* grock-search.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_SEARCH_H
#define GROCK_SEARCH_H

#include <gtk-3.0/gtk/gtk.h>

#include "grock-features.h"

#include "grock-search-popover.h"

// void grock_search_perform_search (GtkSearchEntry* entry, GrockSearchPopover *p);

GrockPoint grock_search_get_lat_lon_from_reference (gchar *reference);

#endif
