/* grock-features-popover.h
 *
 * Copyright © 2020 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtk-3.0/gtk/gtk.h>
#include <champlain-0.12/champlain/champlain.h>

#include "grock-features.h"

G_BEGIN_DECLS

typedef struct
{
  gdouble lat;
  gdouble lon;

  gchar *bng_string;

  gchar *heading;
  gchar *min_specific_time;
  gchar *max_specific_time;
  gchar *min_period;
  gchar *max_period;
  gchar *rock_type;
  gchar *setting;
  gchar *setting_plus;
  gchar *environment;
} GrockFeatureInfo;

#define GROCK_TYPE_FEATURES_POPOVER (grock_features_popover_get_type ())
G_DECLARE_FINAL_TYPE (GrockFeaturesPopover, grock_features_popover, GROCK, FEATURES_POPOVER, GtkPopover)

GrockFeatureInfo *grock_features_popover_get_feature_info (GrockFeaturesPopover *p);
GrockFeaturesPopover *grock_features_popover_new (GtkChamplainEmbed *embed, gdouble x, gdouble y);

G_END_DECLS
