/*
 * Grock - application to display geological maps
 * Copyright © Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Based on code from:
 *    gnome-maps
 *    gnome-recipes
 *    gnome-photos
 */

#include <locale.h>
#include <stdlib.h>

#include "grock-application.h"

#include "grock-about-dialog.h"
#include "grock-config.h"
#include "grock-globals.h"
#include "grock-location.h"
#include "grock-window.h"


struct _GrockApplication
{
  GtkApplication parent_instance;

  GrockWindow        *window;
  GtkAccelGroup      *accels;
};

G_DEFINE_TYPE (GrockApplication, grock_application, GTK_TYPE_APPLICATION)

static void
grock_application_activate (GApplication *application)
{
  GrockApplication *self = GROCK_APPLICATION (application);

  g_assert (GROCK_IS_APPLICATION (self));
  self->window = GROCK_WINDOW (gtk_application_get_active_window (GTK_APPLICATION (self)));

  if (self->window == NULL)
    self->window = grock_window_new (self);

  gtk_window_present (GTK_WINDOW (self->window));
}

static void
on_about_activated (GSimpleAction *action, GVariant *parameter, gpointer app)
{
  grock_application_activate (G_APPLICATION (app));
  GtkWindow *win = gtk_application_get_active_window (GTK_APPLICATION (app));
  grock_window_show_about_dialog (GROCK_WINDOW (win));
}

static void
on_quit_activated (GSimpleAction *action, GVariant *parameter, gpointer app)
{
  GtkWindow *window = gtk_application_get_active_window (GTK_APPLICATION (app));
  gtk_window_close (window);
}

static GActionEntry app_entries[] =
{
  { "about", on_about_activated, NULL, NULL, NULL },
  { "quit",  on_quit_activated,  NULL, NULL, NULL },
};

static void
set_up_actions_and_accels (GApplication *app)
{
  g_action_map_add_action_entries (G_ACTION_MAP (app), app_entries,
                                   G_N_ELEMENTS (app_entries), app);

  struct
    {
      const char *detailed_action;
      const char *accelerators[3];
    } accels[] = {
        { "app.about", { "<Primary>a",               NULL } },
        { "app.quit",  { "<Primary>q", "<Primary>w", NULL } }
    };

  for (gint i = 0; i < G_N_ELEMENTS (accels); ++i)
      gtk_application_set_accels_for_action (GTK_APPLICATION (app),
                                             accels[i].detailed_action,
                                             accels[i].accelerators);
}


static void
grock_application_startup (GApplication *application)
{
  GrockApplication *self = GROCK_APPLICATION (application);

  G_APPLICATION_CLASS (grock_application_parent_class)->startup (G_APPLICATION (self));

  self->accels = gtk_accel_group_new ();
  set_up_actions_and_accels (G_APPLICATION (self));

  self->window = grock_window_new (GROCK_APPLICATION (application));
  grock_window_add_map (self->window);

  grock_location_init ();

}

static void
grock_application_class_init (GrockApplicationClass *class)
{
  GApplicationClass *application_class = G_APPLICATION_CLASS (class);

  application_class->activate = grock_application_activate;
  application_class->startup  = grock_application_startup;

  return;
}

static void
grock_application_init (GrockApplication *self)
{
  return;
}

GrockApplication *
grock_application_new (void)
{
  return g_object_new (GROCK_TYPE_APPLICATION,
                       "application-id", PACKAGE_ID,
                       "flags", G_APPLICATION_FLAGS_NONE,
                       NULL);
}
