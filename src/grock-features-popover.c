/* grock-features-popover.c
 *
 * Copyright © 2020 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "grock-features.h"
#include "grock-features-popover.h"

struct _GrockFeaturesPopover
{
  GtkPopover parent_instance;

  ChamplainView *champlain_view;

  GtkWidget *stack;

  GtkWidget *data_box;
  GtkWidget *spinner_box;

  GtkWidget *title_label;
  GtkWidget *age_label;
  GtkWidget *desc_label;
  GtkWidget *grid_ref_label;

  GtkWidget *spinner;

  GrockFeatureInfo *info;

  // Coords
  gdouble x;
  gdouble y;
};

G_DEFINE_TYPE (GrockFeaturesPopover, grock_features_popover, GTK_TYPE_POPOVER)

/**
 * Takes an ASCII string and makes the first letter of the first word
 * upper case.
 */
static gchar *
cap_first_letter (gchar *str)
{
  gchar *c = str;
  *c = g_ascii_toupper (*c);

  return str;
}

/**
 * Takes an ASCII string and ensures that the first letter of every word is
 * upper case and all other letters are lower case.
 */
static gchar *
cap_first_letter_each (gchar *str)
{
  gchar *c = str;
  *c = g_ascii_toupper (*c);
  ++c;

  while (*c != '\0')
    {
      if (*(c - 1) == ' ')
        {
          ++c;
          continue;
        }
      *c = g_ascii_tolower (*c);
      ++c;
    }

  return str;
}

static void
info_prettify (GrockFeatureInfo *i)
{
  /**
   * Prettify the heading: the section before the hyphen is the formation name
   * and should have the first letter of each word capitalised. The section
   * after the hyphen is the type of rock, and should be all lower case.
   */
  gchar *c = i->heading;
  ++c; // Skip first letter, we need that capitalised
  while (*c != '-' && *c != '\0')
    {
      if (*(c - 1) == ' ')   // Keep the first letter in the word capitalised
        {
          ++c;
          continue;
        }
      *c = g_ascii_tolower (*c);
      ++c;
    }
  while (*c != '\0')
    {
      *c = g_ascii_tolower (*c);
      ++c;
    }

  cap_first_letter_each (i->min_specific_time);
  cap_first_letter_each (i->max_specific_time);
  cap_first_letter_each (i->min_period);
  cap_first_letter_each (i->max_period);
  cap_first_letter (i->setting);
  cap_first_letter (i->setting_plus);

  // If setting_plus has something interesting, add it to setting.
  if (g_strcmp0 (i->setting_plus, "Null") &&
      g_strcmp0 (i->setting_plus, "NULL"))
        i->setting = g_strconcat (i->setting, ". ", i->setting_plus, ".", NULL);
  else  // Otherwise just add a full stop.
        i->setting = g_strconcat (i->setting, ". ", NULL);

}

static void
popover_set_info_data (GrockFeaturesPopover *p, GrockFeatureInfo *info)
{
  gchar *specific_time_str;
  gchar *period_str;

  const gchar *heading_format = "<span size='large' weight='bold'>\%s</span>";
  const gchar *heading_markup = g_markup_printf_escaped (heading_format, info->heading);
  gtk_label_set_markup (GTK_LABEL (p->title_label), heading_markup);

  const gchar *coords_format = "<span size='small' foreground='gray'>   %s</span>";
  const gchar *coords_markup = g_markup_printf_escaped (coords_format, info->bng_string);
  gtk_label_set_markup (GTK_LABEL (p->grid_ref_label), coords_markup);

  if (!g_strcmp0 (info->min_specific_time, info->max_specific_time)) // if they're the same
    specific_time_str = g_strconcat (info->min_specific_time, NULL);
  else
    specific_time_str = g_strconcat (info->min_specific_time, " to ", info->max_specific_time, NULL);

  if (!g_strcmp0 (info->min_period, info->max_period)) // if they're the same
    period_str = g_strconcat (info->min_period, NULL);
  else
    period_str = g_strconcat (info->min_period, " to ", info->min_period, NULL);

  gchar *total_period_str = g_strconcat (period_str, " (", specific_time_str, ")", NULL);

  gtk_label_set_text (GTK_LABEL (p->age_label), total_period_str);
  gtk_label_set_text (GTK_LABEL (p->desc_label), info->setting);
}

static void
grock_features_popover_finalize (GrockFeaturesPopover *self)
{
  g_free (self->info->bng_string);
  g_free (self->info->heading);
  g_free (self->info->min_specific_time);
  g_free (self->info->max_specific_time);
  g_free (self->info->min_period);
  g_free (self->info->max_period);
  g_free (self->info->rock_type);
  g_free (self->info->setting);
  g_free (self->info->environment);

  G_OBJECT_CLASS (grock_features_popover_parent_class)->finalize(G_OBJECT (self));
}

static void
grock_features_popover_init (GrockFeaturesPopover *self)
{
  self->info = grock_feature_info_new ();

  gtk_widget_init_template (GTK_WIDGET (self));
}

static void
grock_features_popover_class_init (GrockFeaturesPopoverClass *klass)
{
  // OVERRIDE FINALIZE
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = grock_features_popover_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/me/leucoso/Grock/grock-features-popover.ui");

  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, stack);

  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, spinner_box);
  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, data_box);

  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, title_label);
  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, age_label);
  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, desc_label);
  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, grid_ref_label);

  gtk_widget_class_bind_template_child (widget_class, GrockFeaturesPopover, spinner);
}

// TODO: implement this mess using GObject properties
GrockFeaturesPopover *
grock_features_popover_new (GtkChamplainEmbed *embed, gdouble x, gdouble y)
{
  ChamplainView *view = gtk_champlain_embed_get_view (embed);
  g_assert (CHAMPLAIN_IS_VIEW (view));

  GrockFeaturesPopover *popover = g_object_new (GROCK_TYPE_FEATURES_POPOVER, NULL);

  g_assert (GROCK_IS_FEATURES_POPOVER (popover));

  popover->champlain_view = view;
  popover->x = x;
  popover->y = y;

  gtk_popover_set_modal (GTK_POPOVER (popover), TRUE);

  GdkRectangle rect = { x, y, 8, 8 };

  gtk_popover_set_relative_to (GTK_POPOVER (popover), GTK_WIDGET (embed));
  gtk_popover_set_pointing_to (GTK_POPOVER (popover), &rect);

  gtk_spinner_start (GTK_SPINNER (popover->spinner));

  gtk_popover_set_constrain_to (GTK_POPOVER (popover), GTK_POPOVER_CONSTRAINT_WINDOW);
  gtk_popover_set_position (GTK_POPOVER (popover), GTK_POS_BOTTOM);
  gtk_popover_popup (GTK_POPOVER (popover));

  gdouble lon = champlain_view_x_to_longitude (view, x);
  gdouble lat = champlain_view_y_to_latitude (view, y);

  GrockPoint point = grock_features_get_web_mercators_from_coords (lat, lon);

  grock_features_get_bng_from_epsg3857 (point.lat, point.lon, popover);
  grock_features_get_from_epsg3857 (point.lat, point.lon, popover);
}

GrockFeatureInfo *
grock_features_popover_get_feature_info (GrockFeaturesPopover *p)
{
  return p->info;
}

void
grock_features_popover_display_info (GrockFeaturesPopover *self)
{
  GrockFeatureInfo *info = self->info;

  info_prettify (info);

  popover_set_info_data (self, info);

  gtk_stack_set_visible_child (GTK_STACK (self->stack), self->data_box);
}
